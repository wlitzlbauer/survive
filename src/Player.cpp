#include "StdAfx.h"
#include "Player.h"

#include "cinder\gl\gl.h"
#include "cinder\gl\Texture.h"
#include "cinder\ImageIo.h"
#include "cinder\app\App.h"

#include "StringUtil.h"
#include "InputManager.h"


using namespace ci;

Player::Player(const ci::Vec2f& _position, float _rotation, int _playerIdx) :
	GameObject(_position, _rotation),
	linearVelocity(0.0f, 0.0f),
	angularVelocity(0.0f),
	playerIdx(_playerIdx)
{
	std::string textureName = StringUtil::format("player%02d.png", playerIdx);
	texture = gl::Texture(ci::loadImage(ci::app::loadAsset(textureName)));
}

Player::~Player()
{

}

void Player::update(float delta)
{
	//TODO: process input and move player.

	position += linearVelocity * delta;
	rotation += angularVelocity * delta;
}

void Player::draw()
{
	gl::pushModelView();
	{
		gl::translate(position);
		gl::rotate(rotation);

		const ci::Vec2f& size = texture.getSize();
		gl::draw(texture, Rectf(Vec2f(-size.x / 2.0f, - size.y), Vec2f(size.x / 2.0f, 0)));
	}
	gl::popModelView();
}

void Player::keyDown(ci::app::KeyEvent event)
{}

void Player::keyUp(ci::app::KeyEvent event)
{}