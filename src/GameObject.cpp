#include "StdAfx.h"
#include "GameObject.h"

GameObject::GameObject(const ci::Vec2f& _position, float _rotation) :
	position(_position),
	rotation(_rotation)
{}

GameObject::~GameObject()
{}