#include "StdAfx.h"
#include "InputManager.h"
#include <stdio.h>

InputManager* InputManager::getInstance() 
{
	static InputManager instance;
	return &instance;
}

InputManager::InputManager()
{
	memset(&lastFrame, 0, sizeof(lastFrame));
	memset(&currentFrame, 0, sizeof(currentFrame));
	memset(&eventFrame, 0, sizeof(eventFrame));
}

void InputManager::update()
{
	memcpy(&lastFrame, &currentFrame, sizeof(currentFrame));
	memcpy(&currentFrame, &eventFrame, sizeof(currentFrame));
}
void InputManager::mouseDown(ci::app::MouseEvent event)
{
}

void InputManager::keyDown(ci::app::KeyEvent event)
{
	eventFrame.keys[event.getCode()] = true;
}
void InputManager::keyUp(ci::app::KeyEvent event)
{
	eventFrame.keys[event.getCode()] = false;
}

void InputManager::bind(const std::string& action, int keyCode, int playerIdx)
{
	CI_ASSERT_MSG(playerIdx < PLAYER_COUNT, "player out of bounds");
	actionBinding[playerIdx][action] = keyCode;
}

void InputManager::unbind(const std::string& action, int playerIdx)
{
	CI_ASSERT_MSG(playerIdx < PLAYER_COUNT, "player out of bounds");
	actionBinding[playerIdx].erase(action);
}

int InputManager::getKeyForAction(const std::string& action, int playerIdx)
{
	CI_ASSERT_MSG(playerIdx < PLAYER_COUNT, "player out of bounds");

	auto it = actionBinding[playerIdx].find(action);

	if (it != actionBinding[playerIdx].end())
	{
		return it->second;
	}

	return 0;
}

bool InputManager::isKeyDown(const std::string& action, int player)
{
	return isKeyDown(getKeyForAction(action, player));
}

bool InputManager::isKeyUp(const std::string& action, int player)
{
	return isKeyUp(getKeyForAction(action, player));
}

bool InputManager::isKeyPressed(const std::string& action, int player)
{
	return isKeyPressed(getKeyForAction(action, player));
}

bool InputManager::isKeyReleased(const std::string& action, int player)
{
	return isKeyReleased(getKeyForAction(action, player));
}

