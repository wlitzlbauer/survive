#include "StdAfx.h"
#include "MainState.h"
#include "cinder/gl/gl.h"
#include "SurviveApp.h"
#include "StdAfx.h"
#include "GameStateManager.h"
#include "InputManager.h"

#include "Player.h"

using namespace ci;
using namespace ci::app;
using namespace std;

void MainState::init()
{
	//TODO: create Player object and add it to gameObjects
}

void MainState::exit()
{
	// destroy game objects
	gameObjects.clear();
}

void MainState::update(float delta)
{
	// return to MainState when ESC is released.

	for (auto gameObject : gameObjects)
	{
		gameObject->update(delta);
	}
}

void MainState::draw()
{
	app->drawText("This is my awesome game", app->getWindowSize() / 2, true);

	for (auto gameObject : gameObjects)
	{
		gameObject->draw();
	}
}