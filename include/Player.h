#pragma once

#include "GameObject.h"
#include "cinder/gl/Texture.h"
#include "cinder/app/App.h"
#include "cinder/ImageIo.h"

class Player :
	public GameObject
{
public:
	Player(const ci::Vec2f& _position, float _rotation, int _playerIdx);
	virtual ~Player();

	virtual void update(float delta);
	virtual void draw();

	virtual void keyDown(ci::app::KeyEvent event);
	virtual void keyUp(ci::app::KeyEvent event);

private:
	int playerIdx;

	ci::Vec2f linearVelocity;
	float angularVelocity;

	ci::gl::Texture texture;
};